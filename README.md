# DRY Repository - A cheatsheet on reusable components in GitLab



## About

Over 20 years ago, the book The Programmatic Programmer brought attention to the DRY principle, or “Don’t Repeat Yourself’. This principle is defined as every piece of knowledge must have a single unambiguous, authoritative representation within a system.


The main problem to solve here is minimizing duplication. As your development project is bombarded with new requests or changing requirements, you are balancing between development of net-new features or maintaining existing code. The important part is how to reduce duplicate knowledge across your projects or codebase.


In this post, we explore mechanisms throughout GitLab that leverage the DRY principle to cut down on code duplication and standardize on knowledge.

The officially distributed Gitlab CI templates can be found online in the [lib/gitlab/ci/templates folder](https://docs.gitlab.com/ee/ci/yaml/index.html#include) of the GitLab project.  See the [.gitlab-ci.yml](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/dry-repository-a-cheatsheet/-/blob/main/.gitlab-ci.yml) in this project for examples on how to leverage them in your own pipelines.
